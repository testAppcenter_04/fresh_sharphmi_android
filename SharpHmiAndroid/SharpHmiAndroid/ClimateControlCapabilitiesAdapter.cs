﻿
using System;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using HmiApiLib.Common.Structs;

namespace SharpHmiAndroid
{
    internal class ClimateControlCapabilitiesAdapter: BaseAdapter<ClimateControlCapabilities>
    {
        private Activity context;
        private List<ClimateControlCapabilities> climateControlCapabilitiesList;

        public ClimateControlCapabilitiesAdapter(Activity activity, List<ClimateControlCapabilities> climateControlCapabilitiesList)
        {
            this.context = activity;
            this.climateControlCapabilitiesList = climateControlCapabilitiesList;
        }

        public override ClimateControlCapabilities this[int position] => climateControlCapabilitiesList[position];

        public override int Count => climateControlCapabilitiesList.Count;

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var view = context.LayoutInflater.Inflate(Resource.Layout.touch_event_item_adapter, parent, false);

            var text = view.FindViewById<TextView>(Resource.Id.touch_event_count);
            text.Text = climateControlCapabilitiesList[position].getModuleName().ToString();
            return view;
        }
    }
}