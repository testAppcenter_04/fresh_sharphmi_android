﻿using System.Collections.Generic;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Java.IO;
using Java.Util;

namespace SharpHmiAndroid
{
    [Activity(Label = "FileChooserActivity")]
    public class FileChooserActivity : ListActivity
    {
        private static string LOG_TAG = typeof(FileChooserActivity).Name;

		private List<string> fileListItem = null;
		private List<string> path = null;
		private string rootPath = null;
		private TextView currentPath;

    	protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.filechooser);
            currentPath = (TextView)FindViewById(Resource.Id.filechooser_path);

            string state = Environment.ExternalStorageState;

            if (Environment.MediaMounted.Equals(state))
			{
				rootPath = Environment.ExternalStorageDirectory.Path;
			}

			if (rootPath != null)
			{
				getDirectoryContents(rootPath);
			}
		}

		private void getDirectoryContents(string directoryPath)
		{
			if (directoryPath == null)
			{
				return;
			}

			currentPath.Text = "Path: " + directoryPath;
			fileListItem = new List<string>();
			path = new List<string>();
			File currentFile = new File(directoryPath);
			File[] filesInFolder = currentFile.ListFiles();

			if (!directoryPath.Equals(rootPath))
			{
				fileListItem.Add(rootPath);
				path.Add(rootPath);
				fileListItem.Add("../");
				path.Add(currentFile.Parent);
			}

            for (int i = 0; i < filesInFolder.Length; i++)
			{
				File file = filesInFolder[i];

				if (!file.IsHidden && file.CanRead())
				{
					path.Add(file.Path);
					if (file.IsDirectory)
					{
						fileListItem.Add(file.Name + "/");
					}
					else
					{
						fileListItem.Add(file.Name);
					}
				}
			}

			Collections.Sort(fileListItem);
			Collections.Sort(path);
            ArrayAdapter<string> fileList = new ArrayAdapter<string>(this, Resource.Layout.filerow, fileListItem);
            ListAdapter = fileList;
		}

        protected override void OnListItemClick(ListView l, View v, int position, long id)
		{
            if ((path != null) && (position < path.Count))
			{
				File file = new File(path[position]);

				if (file.IsDirectory)
				{
					if (file.CanRead())
					{
						getDirectoryContents(path[position]);
					}
					else
					{
                        new AlertDialog.Builder(this).SetIcon(Resource.Drawable.icon).SetTitle("Can't read folder!").SetPositiveButton("OK", (senderAlert, args) =>{}).Show();
					}
				}
				else
				{
					IntentHelper.addObjectForKey(file.Path, Const.INTENTHELPER_KEY_FILECHOOSER_FILE);
                    SetResult(Result.Ok);
					Finish();
				}
			}
		}

	}
}
